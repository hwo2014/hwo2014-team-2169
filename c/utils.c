#include "utils.h"

#include <errno.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

char output_buf[OUTPUT_BUF_SIZE];

void error(char *fmt, ...) {
  char buf[BUFSIZ];

  va_list ap;
  va_start(ap, fmt);
  vsnprintf(buf, BUFSIZ, fmt, ap);
  va_end(ap);

  if (errno) {
    perror(buf);
  } else {
    fprintf(stderr, "ERROR: %s\n", buf);
  }

  exit(1);
}

int connect_to(char *hostname, char *port) {
  int status;
  int fd;
  int flag;
  char portstr[32];
  struct addrinfo hint;
  struct addrinfo *info;

  memset(&hint, 0, sizeof(struct addrinfo));
  hint.ai_family = PF_INET;
  hint.ai_socktype = SOCK_STREAM;

  sprintf(portstr, "%d", atoi(port));

  status = getaddrinfo(hostname, portstr, &hint, &info);
  if (status != 0) {
    error("Failed to get address: %s", gai_strerror(status));
  }

  fd = socket(PF_INET, SOCK_STREAM, 0);
  if (fd < 0) {
    error("Failed to create a socket.");
  }

  /* Disable Nagle's Algorithm to send our messages ASAP. */
  status = setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(flag));
  if (status < 0) {
    error("Failed to set TCP_NODELAY.");
  }

  status = connect(fd, info->ai_addr, info->ai_addrlen);
  if (status < 0) {
    error("Failed to connect to the server.");
  }

  freeaddrinfo(info);
  return fd;
}

void copy_alloced_str(char **dest, char *src) {
  *dest = malloc((strlen(src) + 1) * sizeof(char));
  strcpy(*dest, src);
}

long time_diff_us(struct timeval *t1, struct timeval *t0) {
  return (t1->tv_sec - t0->tv_sec) * 1000000L + (t1->tv_usec - t0->tv_usec);
}

double get_eff_radius(int piece_index, int start_lane) {
  piece_t *the_piece = &the_track.pieces[piece_index];
  if (the_piece->type == BENT_PIECE) {
    double angle = the_piece->angle;
    /* TODO: Adjust this when we're switching lanes. */
    double lane_dist = the_track.lane_dists[start_lane];
    /* Note: Left-turns have negative angles and left-lanes have negative
     * lane-distances. */
    return the_piece->radius +
      (same_sign(angle, lane_dist) ? -fabs(lane_dist) : +fabs(lane_dist));
  } else {
    return 0.0;
  }
}

double get_piece_length(int piece_index, int start_lane, int end_lane) {
  piece_t *the_piece = &the_track.pieces[piece_index];
  if (the_piece->type == STRAIGHT_PIECE) {
    if (start_lane != end_lane) {
      double lane_dist_covered =
        fabs(the_track.lane_dists[start_lane] - the_track.lane_dists[end_lane]);
      return sqrt(the_piece->length * the_piece->length +
          lane_dist_covered * lane_dist_covered);
    } else {
      return the_piece->length;
    }
  } else {
    double radius = get_eff_radius(piece_index, start_lane);
    return arc_len(radius, the_piece->angle);
  }
}

double get_distance(car_pos_t *from, car_pos_t *to) {
  if (from->piece_index == to->piece_index) {
    return fabs(to->in_piece_dist - from->in_piece_dist);
  } else {
    double dist =
      get_piece_length(from->piece_index, from->start_lane, from->end_lane) -
      from->in_piece_dist;
    int next_piece = next_piece_index(from->piece_index);
    while (next_piece != to->piece_index) {
      dist += get_piece_length(next_piece, to->start_lane, to->start_lane);
      next_piece = next_piece_index(next_piece);
    }
    dist += to->in_piece_dist;
    return dist;
  }
}
