/* Team TimePass's little car-bot for HelloWorldOpen 2014.
 *
 * Started with the initial skeletal code (constant throttle car) in C as base
 * and then modified heavily.
 *
 * If defined, the following environment-variables affect game-play:
 *   HWO_TRACK - code-name of the track to join instead of the default track.
 */
#include "cbot.h"

#include <math.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>

#include "handlers.h"
#include "linreg.h"
#include "msgs.h"
#include "utils.h"

#define EPSILON 0.0001

/* Uncomment this to use machine-learning (ml_xxx() functions) instead of
 * reverse-engineering (re_yyy() functions) for predicting the physics of the
 * race. */
/* #define USE_ML 1 */

/* When using a learned physics model to predict something, bail out after
 * these many iterations as we don't seem to be getting a proper answer. */
#define MAX_PRED_ITERATIONS 1000

/* Uncomment this to gather statistics and to terminate after enough data has
 * been acquired. */
/* #define GATHER_STATS 1 */

/* Empirical evidence suggests that this is a safe throttle-value for
 * straight and bent pieces, until we have a better model of the physics. */
#define SAFE_THROTTLE 0.55

/* Based on empirical observations across tracks, the car seems to get
 * de-slotted when the slip-angle reaches this threshold. */
#define DEFAULT_DE_SLOT_SLIP_ANGLE 60.0

#define FULL_COVERAGE 1.0

/* If we've covered less than this much of a straight piece, don't worry about
 * what's the type of the next piece. */
#define FREE_ON_STRAIGHT 0.70

/* Until we've covered this much of a bent piece, don't bother speeding up. */
#define CONSTRAIN_ON_BENT 0.80

track_t the_track;
car_t my_car;
turbo_t turbo;

car_state_t last_state;
car_state_t curr_state;

static double last_throttle = 0.0;

#ifdef USE_ML
typedef struct {
  model_t model;
  int num_pieces;
  sample_data_t sample_data;
} piece_model_t;

static model_state_t training_state = READY_TO_TRAIN;
static piece_model_t straight_physics;
static piece_model_t bent_physics;
static int *piece_sample_buckets = NULL;
#else /* !USE_ML */
double init_straight_power = FULL_THROTTLE;
double k_by_m;
double v_1;
static model_state_t straight_phy_state = READY_TO_TRAIN;
static model_state_t bent_phy_state = READY_TO_TRAIN;

#define MAX_RE_SAMPLES 10
static int num_straight_samples = 0;
static double straight_sample_speeds[MAX_RE_SAMPLES];

static int num_bent_samples = 0;
static double bent_sample_speeds[MAX_RE_SAMPLES];
#endif /* USE_ML */

static double de_slot_slip_angle = DEFAULT_DE_SLOT_SLIP_ANGLE;
static double max_speed = 0.0;

#ifdef GATHER_STATS
#define NUM_STATS_SAMPLES 200
static long read_times[NUM_STATS_SAMPLES];
static long proc_times[NUM_STATS_SAMPLES];

static void print_stats(void) {
  long min_read = LONG_MAX, max_read = LONG_MIN;
  long long sum_read = 0LL;
  long min_proc = LONG_MAX, max_proc = LONG_MIN;
  long long sum_proc = 0LL;
  int i;
  for (i = 0; i < NUM_STATS_SAMPLES; ++i) {
    sum_read += read_times[i];
    if (read_times[i] < min_read) {
      min_read = read_times[i];
    }
    if (read_times[i] > max_read) {
      max_read = read_times[i];
    }

    sum_proc += proc_times[i];
    if (proc_times[i] < min_proc) {
      min_proc = proc_times[i];
    }
    if (proc_times[i] > max_proc) {
      max_proc = proc_times[i];
    }
  }
  printf("Read times: min=%ld, max=%ld, avg=%ld (all in usecs).\n", min_read,
      max_read, (long)(sum_read / NUM_STATS_SAMPLES));
  printf("Proc times: min=%ld, max=%ld, avg=%ld (all in usecs).\n", min_proc,
      max_proc, (long)(sum_proc / NUM_STATS_SAMPLES));
}
#endif /* GATHER_STATS */

void reset_car_state(car_status_t status) {
  last_state.status = IDLE_CAR;
  last_state.tick = 0;
  last_state.speed = 0.0;
  last_state.delta_speed = 0.0;
  last_state.delta_slip_angle = 0.0;
  last_state.delta_delta_slip_angle = 0.0;
  last_state.using_turbo = 0;

  curr_state.status = status;
  curr_state.tick = 0;
  curr_state.speed = 0.0;
  curr_state.delta_speed = 0.0;
  curr_state.delta_slip_angle = 0.0;
  curr_state.delta_delta_slip_angle = 0.0;
  curr_state.using_turbo = 0;
}

static cJSON *get_throttle_msg(double throttle) {
  last_throttle = throttle;
  return throttle_msg(throttle, curr_state.tick);
}

#ifdef USE_ML
/* Positive value of the drift if it is increasing, a random negative number
 * otherwise. */
static double ml_get_drift(void) {
  double drift = -1.0;
  if (same_sign(curr_state.pos.slip_angle, last_state.pos.slip_angle)) {
    drift = fabs(curr_state.pos.slip_angle) - fabs(last_state.pos.slip_angle);
  }
  return drift;
}

static void ml_initialize(void) {
  training_state = READY_TO_TRAIN;
  piece_sample_buckets = NULL;

  init_model(&straight_physics.model, "straight_piece",
      DELTA_DELTA_SLIP_ANGLE_FEAT + 1);
  straight_physics.num_pieces = 0;
  straight_physics.sample_data.num_samples = 0;

  init_model(&bent_physics.model, "bent_piece",
      SPEED_SQUARED_BY_INVERSE_RADIUS_FEAT + 1);
  bent_physics.num_pieces = 0;
  bent_physics.sample_data.num_samples = 0;
}

static void ml_clean_up(void) {
  free(piece_sample_buckets);
  clean_model(&straight_physics.model);
  clean_model(&bent_physics.model);
}

static void ml_analyze_track(void) {
  int i;
  piece_sample_buckets = malloc(the_track.num_pieces * sizeof(int));
  straight_physics.num_pieces = 0;
  bent_physics.num_pieces = 0;
  for (i = 0; i < the_track.num_pieces; ++i) {
    if (the_track.pieces[i].type == STRAIGHT_PIECE) {
      piece_sample_buckets[i] = straight_physics.num_pieces++;
    } else {
      piece_sample_buckets[i] = bent_physics.num_pieces++;
    }
  }
  printf("Track \"%s\" has %d straight and %d bent pieces.\n",
      the_track.id, straight_physics.num_pieces, bent_physics.num_pieces);
}

static void collect_features(features_t *features, car_state_t *car_state,
    double throttle) {
  piece_t curr_piece = the_track.pieces[car_state->pos.piece_index];
  double speed = car_state->speed;

  features->x[UNIT_FEAT] = 1.0;
  features->x[SPEED_FEAT] = speed;
  features->x[SPEED_SQUARED_FEAT] = speed * speed;
  features->x[DELTA_SPEED_FEAT] = car_state->delta_speed;
  features->x[THROTTLE_FEAT] =
    (car_state->using_turbo == 1) ? (throttle * turbo.boost) : throttle;
  features->x[SLIP_ANGLE_FEAT] = car_state->pos.slip_angle;
  features->x[DELTA_SLIP_ANGLE_FEAT] = car_state->delta_slip_angle;
  features->x[DELTA_DELTA_SLIP_ANGLE_FEAT] = car_state->delta_delta_slip_angle;
 
  if (curr_piece.type == STRAIGHT_PIECE) {
    features->x[BEND_ANGLE_FEAT] = 0.0;
    features->x[INVERSE_RADIUS_FEAT] = 0.0;
    features->x[SPEED_SQUARED_BY_INVERSE_RADIUS_FEAT] = 0.0;
  } else {
    double radius =
      get_eff_radius(car_state->pos.piece_index, car_state->pos.start_lane);
    features->x[BEND_ANGLE_FEAT] = (curr_piece.angle >= 0.0) ? +1.0 : -1.0;
    features->x[INVERSE_RADIUS_FEAT] = 1.0 / radius;
    features->x[SPEED_SQUARED_BY_INVERSE_RADIUS_FEAT] =
      (speed * speed) / radius;
  }
}

static void collect_sample(void) {
  if (curr_state.tick == (last_state.tick + 1)) {
    int piece_index = last_state.pos.piece_index;
    int sample_index, piece_bucket;

    piece_model_t *piece_model = &straight_physics;
    if (the_track.pieces[piece_index].type == BENT_PIECE) {
      piece_model = &bent_physics;
    }
    piece_bucket = piece_sample_buckets[piece_index];
    sample_index = piece_model->sample_data.num_samples;
    /* Keep filling up the samples - if we run out of space, wrap around and
     * spread the samples as uniformly as possible. */
    if (sample_index >= MAX_TRAINING_SAMPLES) {
      sample_index %= MAX_TRAINING_SAMPLES;
      sample_index = (sample_index * piece_model->num_pieces + piece_bucket) %
        MAX_TRAINING_SAMPLES;
    }
    /* Sanity check. */
    if (sample_index < 0 || sample_index >= MAX_TRAINING_SAMPLES) {
      snprintf(output_buf, OUTPUT_BUF_SIZE,
          "Sample-index %d out of range.\n", sample_index);
      error(output_buf);
    }
    collect_features(
        &(piece_model->sample_data.samples[sample_index].features),
        &last_state, last_throttle);
    piece_model->sample_data.samples[sample_index].obs[OBS_SPEED] =
      curr_state.speed;
    piece_model->sample_data.samples[sample_index].obs[OBS_SLIP_ANGLE] =
      curr_state.pos.slip_angle;

    /* Must clip this later before trying to train the models. */
    piece_model->sample_data.num_samples++;
  }
}

static double ml_max_poss_speed(void) {
  car_state_t state;
  features_t features;
  piece_model_t *piece_model = &straight_physics;
  double new_speed, new_slip_angle;
  int piece_index = 0;
  piece_type_t piece_type = STRAIGHT_PIECE;
  int i = 0;

  while (the_track.pieces[piece_index].type == BENT_PIECE &&
      piece_index < the_track.num_pieces) {
    ++piece_index;
  }
  if (the_track.pieces[piece_index].type == BENT_PIECE) {
    piece_type = BENT_PIECE;
    piece_model = &bent_physics;
  }
  state.status = RACING_CAR;
  state.tick = 1;
  state.pos.slip_angle = 0.0;
  state.pos.piece_index = piece_index;
  state.pos.in_piece_dist = 0.0;
  state.pos.piece_length = get_piece_length(piece_index, 0, 0);
  state.pos.start_lane = 0;
  state.pos.end_lane = 0;
  state.pos.lap = 1;
  state.speed = 1.0;
  state.delta_speed = 0.0;
  state.delta_slip_angle = 0.0;
  state.delta_delta_slip_angle = 0.0;
  state.using_turbo = 0;

  while (i < MAX_PRED_ITERATIONS) {
    collect_features(&features, &state, FULL_THROTTLE);
    predict(&piece_model->model, &features, &new_speed, &new_slip_angle);
    /*
    printf("Speed & slip-angle @%d: %.2f %+.2f\n", i, new_speed,
        new_slip_angle);
    */

    state.delta_speed = new_speed - state.speed;
    if (fabs(state.delta_speed) <= EPSILON) {
      break;
    }
    if (piece_type == BENT_PIECE &&
        (fabs(new_slip_angle) >= de_slot_slip_angle)) {
      break;
    }
    state.delta_delta_slip_angle =
      (new_slip_angle - state.pos.slip_angle) - state.delta_slip_angle;
    state.delta_slip_angle = new_slip_angle - state.pos.slip_angle;
    state.pos.slip_angle = new_slip_angle;
    state.speed = new_speed;
    ++i;
  }
  if (i == MAX_PRED_ITERATIONS) {
    printf("WARNING: max_poss_speed() did not converge.\n");
  }
  printf("Maximum possible speed: %.2f\n", state.speed);
  return state.speed;
}

void ml_lap_done(int lap_num, int in_ticks) {
  if (training_state == READY_TO_TRAIN) {
    straight_physics.sample_data.num_samples =
      min_of(straight_physics.sample_data.num_samples, MAX_TRAINING_SAMPLES);
    printf("Training straight-piece physics: %d samples, %d features each.\n",
        straight_physics.sample_data.num_samples,
        straight_physics.model.num_used_features);
    train_model(&straight_physics.model, &straight_physics.sample_data);

    bent_physics.sample_data.num_samples =
      min_of(bent_physics.sample_data.num_samples, MAX_TRAINING_SAMPLES);
    printf("Training bent-piece physics: %d samples, %d features each.\n",
        bent_physics.sample_data.num_samples,
        bent_physics.model.num_used_features);
    train_model(&bent_physics.model, &bent_physics.sample_data);

    training_state = READY_TO_PREDICT;

    max_speed = ml_max_poss_speed();
  }
}

static cJSON *ml_get_trained_response(void) {
  double power;
  double drift = ml_get_drift();

  if (drift > 0.0) {
    double norm_drift = drift / 90.0;
    power = 0.5 - norm_drift / (1.0 + norm_drift);
  } else {
    piece_t curr_piece = the_track.pieces[curr_state.pos.piece_index];
    piece_t next_piece =
      the_track.pieces[next_piece_index(curr_state.pos.piece_index)];
    double piece_coverage =
      curr_state.pos.in_piece_dist / curr_state.pos.piece_length;

    if (curr_piece.type == STRAIGHT_PIECE) {
      if (piece_coverage < FREE_ON_STRAIGHT ||
          next_piece.type == STRAIGHT_PIECE) {
        power = FULL_THROTTLE;
      } else {
        if (curr_state.speed > 5.5) {
          power = NO_THROTTLE;
        } else {
          double damper = (piece_coverage - FREE_ON_STRAIGHT) /
            (FULL_COVERAGE - FREE_ON_STRAIGHT);
          power = FULL_THROTTLE - damper * (FULL_THROTTLE - NO_THROTTLE);
        }
      }
    } else {
      if (piece_coverage < CONSTRAIN_ON_BENT ||
          next_piece.type == BENT_PIECE) {
        power = 0.6 * FULL_THROTTLE;
      } else {
        double booster = (piece_coverage - CONSTRAIN_ON_BENT) /
          (FULL_COVERAGE - CONSTRAIN_ON_BENT);
        power = (0.60 + booster * 0.25) * FULL_THROTTLE;
      }
    }
  }

  /* Our floating-point calculations might yield invalid values that need to
   * be clipped before being sent to the server. */
  if (power > FULL_THROTTLE) {
    printf("WARNING: Clipping excess throttle power %.2f at tick %d.\n", power,
        curr_state.tick);
    power = FULL_THROTTLE;
  } else if (power < NO_THROTTLE) {
    printf("WARNING: Ignoring insufficient throttle power %.2f.\n", power);
    power = NO_THROTTLE;
  }

  return get_throttle_msg(power);
}

cJSON *ml_get_race_response(void) {
  if (training_state == READY_TO_TRAIN) {
    double power = 0.0;
    if (fabs(curr_state.pos.slip_angle) < (0.5 * de_slot_slip_angle)) {
      double fraction_done =
        (double)(curr_state.pos.piece_index + 1) / (double)the_track.num_pieces;
      double damper = cos(deg2rad(fabs(curr_state.pos.slip_angle)));
      /* Gradually move from (low * max_throttle) to max_throttle:
       *   max_throttle * (low + (1 - low) * done)
       */
      power = damper * FULL_THROTTLE * (0.30 + 0.70 * fraction_done);
    }
    collect_sample();
    return get_throttle_msg(power);
  } else {
    return ml_get_trained_response();
  }
}
#else /* !USE_ML */
static void re_initialize(void) {
  straight_phy_state = READY_TO_TRAIN;
  bent_phy_state = READY_TO_TRAIN;
  num_straight_samples = 0;
  num_bent_samples = 0;

  init_straight_power = FULL_THROTTLE;
}

/* On a straight piece, the damping seems to be linearly proportional to
 * the speed v in a discrete simulation:
 *   m * (v[t] - v[t-1]) = F[t-1] - k * v[t-1]
 * where k is the damping factor, m is the mass and F[i] is the constant force
 * due to a constant throttle. If v[0] is 0:
 *   F[0] = m * v[1]
 * and we can rewrite the earlier equation (assuming constant throttle) as:
 *   v[t] = v[1] + v[t-1] * (1 - X)
 * where X = k / m and therefore:
 *   X = 2 - (v[2] / v[1])
 * If F[i] is proportional to h[i], the applied throttle, then:
 *   F[i] = C * h[i]
 * and:
 *   C = F[0] / h[0] = m * v[1] / h[0]
 * and after substitution and simplification:
 *   v[t] = v[1] * (h[t-1] / h[0]) + v[t-1] * (1 - X)
 */
static double re_pred_next_speed(double curr_speed, double power) {
  return v_1 * (power / init_straight_power) + curr_speed * (1.0 - k_by_m);
}

/* Since:
 *   v[t] - v[t-1] = (F[t-1] / m) - X * v[t-1]
 * where X = k / m and at terminal speed v[t] = v[t-1], the terminal speed for
 * a given throttle h is:
 *   v[T] = (F[T] / m) / X = (C * h[T] / m) / X = (v[1] * h[T] / h[0]) / X
 */
static double re_pred_term_speed(double power) {
  return (v_1 * power / init_straight_power) / k_by_m;
}

static void re_train_straight(void) {
  /* Sanity check. */
  if (straight_sample_speeds[0] > EPSILON) {
    snprintf(output_buf, OUTPUT_BUF_SIZE,
        "Straight-piece sample-speed at 0 is not 0 (%.2f).\n",
        straight_sample_speeds[0]);
    error(output_buf);
  }
  k_by_m = 2.0 - (straight_sample_speeds[2] / straight_sample_speeds[1]);
  v_1 = straight_sample_speeds[1];
  /* Verify predictions. */
  {
    int i;
    for (i = 2; i < MAX_RE_SAMPLES; ++i) {
      double pred_speed =
        re_pred_next_speed(straight_sample_speeds[i - 1], init_straight_power);
      if (fabs(pred_speed - straight_sample_speeds[i]) > EPSILON) {
        snprintf(output_buf, OUTPUT_BUF_SIZE,
            "Straight-piece model is invalid: v[%d]=%.2f, pred=%.2f\n", i,
            straight_sample_speeds[i], pred_speed);
        error(output_buf);
      }
    }
    printf("Straight-piece model is valid.\n");
  }
  max_speed = re_pred_term_speed(FULL_THROTTLE);
  printf("Maximum possible speed: %.2f\n", max_speed);
}

static void re_train_bent(void) {
}

cJSON *re_get_race_response(void) {
  piece_t curr_piece = the_track.pieces[curr_state.pos.piece_index];
  if (curr_piece.type == STRAIGHT_PIECE &&
      straight_phy_state == READY_TO_TRAIN) {
    straight_sample_speeds[num_straight_samples++] = curr_state.speed;
    if (num_straight_samples == MAX_RE_SAMPLES) {
      re_train_straight();
      straight_phy_state = READY_TO_PREDICT;
    }
    return get_throttle_msg(init_straight_power);
  }
  if (curr_piece.type == BENT_PIECE &&
      bent_phy_state == READY_TO_TRAIN) {
    bent_sample_speeds[num_bent_samples++] = curr_state.speed;
    if (num_bent_samples == MAX_RE_SAMPLES) {
      re_train_bent();
      bent_phy_state = READY_TO_PREDICT;
    }
    return get_throttle_msg(SAFE_THROTTLE);
  }
  return get_throttle_msg(SAFE_THROTTLE);
}
#endif /* USE_ML */

static void initialize(void) {
  init_msgs();

#ifdef USE_ML
  printf("Using machine-learning for race-physics.\n");
  ml_initialize();
#else
  printf("Using reverse-engineering for race-physics.\n");
  re_initialize();
#endif /* USE_ML */

  reset_car_state(IDLE_CAR);

  the_track.id = NULL;
  the_track.num_pieces = 0;
  the_track.pieces = NULL;
  the_track.num_lanes = 0;
  the_track.lane_dists = NULL;

  my_car.name = NULL;
  my_car.color = NULL;
  my_car.length = 0.0;
  my_car.width = 0.0;
  my_car.guide_flag_pos = 0.0;

  turbo.boost = DEFAULT_TURBO;
  turbo.expiry_tick = INT_MAX;
}

static void clean_up(void) {
  close_msgs();

#ifdef USE_ML
  ml_clean_up();
#endif /* USE_ML */

  free(my_car.name);
  free(my_car.color);

  free(the_track.id);
  free(the_track.pieces);
  free(the_track.lane_dists);
}

void analyze_track(void) {
#ifdef USE_ML
  ml_analyze_track();
#endif /* USE_ML */
}

void car_crashed(void) {
  de_slot_slip_angle = fabs(curr_state.pos.slip_angle);
}

cJSON *get_race_response(void) {
#ifdef USE_ML
  return ml_get_race_response();
#else
  return re_get_race_response();
#endif /* USE_ML */
}

void lap_done(int lap_num, int in_ticks) {
#ifdef USE_ML
  ml_lap_done(lap_num, in_ticks);
#endif /* USE_ML */
}

int main(int argc, char *argv[]) {
  int sock_fd;
  cJSON *in_json, *out_json;
  cJSON *msg_type;

#ifdef GATHER_STATS
  int sample_num = 0;
  struct timeval start_time, start_proc_time, end_time;
#endif /* GATHER_STATS */

  if (argc != 5) {
    error("Usage: bot host port botname botkey\n");
  }

  initialize();

  sock_fd = connect_to(argv[1], argv[2]);

  out_json = join_msg(argv[3], argv[4]);
  write_msg(sock_fd, out_json);
  cJSON_Delete(out_json);

#ifdef GATHER_STATS
  gettimeofday(&start_time, NULL);
#endif /* GATHER_STATS */
  while ((in_json = read_msg(sock_fd)) != NULL) {
    char *msg_type_name;

#ifdef GATHER_STATS
    gettimeofday(&start_proc_time, NULL);
#endif /* GATHER_STATS */

    msg_type = get_field(in_json, "msgType", "Missing msgType field.");

    /* We expect to see _far more_ "carPositions" messages than anything else,
     * so just checking for it first is enough for now.
     */
    msg_type_name = msg_type->valuestring;
    if (!strcmp("carPositions", msg_type_name)) {
      out_json = handle_car_positions(in_json);
    } else if (!strcmp("join", msg_type_name)) {
      out_json = handle_join(in_json);
    } else if (!strcmp("joinRace", msg_type_name)) {
      out_json = handle_join_race(in_json);
    } else if (!strcmp("yourCar", msg_type_name)) {
      out_json = handle_your_car(in_json);
    } else if (!strcmp("gameInit", msg_type_name)) {
      out_json = handle_game_init(in_json);
    } else if (!strcmp("gameStart", msg_type_name)) {
      out_json = handle_game_start(in_json);
    } else if (!strcmp("turboAvailable", msg_type_name)) {
      out_json = handle_turbo_available(in_json);
    } else if (!strcmp("turboStart", msg_type_name)) {
      out_json = handle_turbo_start(in_json);
    } else if (!strcmp("turboEnd", msg_type_name)) {
      out_json = handle_turbo_end(in_json);
    } else if (!strcmp("lapFinished", msg_type_name)) {
      out_json = handle_lap_finished(in_json);
    } else if (!strcmp("finish", msg_type_name)) {
      out_json = handle_finish(in_json);
    } else if (!strcmp("gameEnd", msg_type_name)) {
      out_json = handle_game_end(in_json);
    } else if (!strcmp("tournamentEnd", msg_type_name)) {
      out_json = handle_tournament_end(in_json);
    } else if (!strcmp("dnf", msg_type_name)) {
      out_json = handle_dnf(in_json);
    } else if (!strcmp("crash", msg_type_name)) {
      out_json = handle_crash(in_json);
    } else if (!strcmp("spawn", msg_type_name)) {
      out_json = handle_spawn(in_json);
    } else if (!strcmp("error", msg_type_name)) {
      out_json = handle_error(in_json);
    } else {
      out_json = handle_unknown(in_json);
    }

    if (out_json != NULL) {
      write_msg(sock_fd, out_json);
      purge_sent_msg(out_json);
    }

    cJSON_Delete(in_json);

#ifdef GATHER_STATS
    gettimeofday(&end_time, NULL);
    read_times[sample_num] = time_diff_us(&start_proc_time, &start_time);
    proc_times[sample_num] = time_diff_us(&end_time, &start_proc_time);
    sample_num++;
    if (sample_num == NUM_STATS_SAMPLES) {
      printf("Have enough samples (%d); aborting program.\n",
          NUM_STATS_SAMPLES);
      print_stats();
      exit(0);
    }
    gettimeofday(&start_time, NULL);
#endif /* GATHER_STATS */
  }

  clean_up();

  return 0;
}
