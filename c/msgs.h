#ifndef _MSGS_H_INCLUDED_
#define _MSGS_H_INCLUDED_

#include <stdlib.h>

#include "cJSON.h"

extern void init_msgs(void);
extern void close_msgs(void);

extern cJSON *get_field(cJSON *obj, char *field, char *err_msg);
extern cJSON *get_item_at(cJSON *array, int index, char *err_msg);

extern cJSON *ping_msg(void);
extern cJSON *join_msg(char *bot_name, char *bot_key);
extern cJSON *throttle_msg(double value, int at_tick);
extern cJSON *turbo_msg(void);
extern cJSON *make_msg(char *type, cJSON *msg);

extern cJSON *read_msg(int fd);
extern void write_msg(int fd, cJSON *msg);

extern void purge_sent_msg(cJSON *sent_msg);

#endif /* _MSGS_H_INCLUDED_ */
