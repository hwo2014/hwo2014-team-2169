#ifndef _UTILS_H_INCLUDED_
#define _UTILS_H_INCLUDED_

#include <math.h>
#include <sys/time.h>

#include "cbot.h"

#define same_sign(x,y) (((x) * (y)) >= 0)
#define deg2rad(x) ((x) * M_PI / 180.0)
#define arc_len(radius, degrees) ((radius) * fabs(deg2rad(degrees)))
#define max_of(x,y) (((x) > (y)) ? (x) : (y))
#define min_of(x,y) (((x) < (y)) ? (x) : (y))

#define OUTPUT_BUF_SIZE 2048
extern char output_buf[OUTPUT_BUF_SIZE];

extern void error(char *fmt, ...);
extern int connect_to(char *hostname, char *port);
extern void copy_alloced_str(char **dest, char *src);
extern long time_diff_us(struct timeval *t1, struct timeval *t0);

extern double get_eff_radius(int piece_index, int start_lane);
extern double get_piece_length(int piece_index, int start_lane, int end_lane);
extern double get_distance(car_pos_t *from, car_pos_t *to);

#endif /* _UTILS_H_INCLUDED_ */
