#include "msgs.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "utils.h"

#define INIT_BUFF_SIZE 4096

static size_t read_buff_size = 0;
static char *read_buff = NULL;

static cJSON *ping = NULL;
static char ping_str[] = "{\"msgType\":\"ping\"}\n";
static size_t ping_str_len = 0;

static cJSON *throttle = NULL;
static double throttle_val = 0.0;
static int throttle_tick = 0;
static char throttle_str[INIT_BUFF_SIZE];

static cJSON *use_turbo = NULL;
static char use_turbo_str[] =
    "{\"msgType\":\"turbo\",\"data\":\"Bhaag beta bhaag!\"}\n";
static size_t use_turbo_str_len = 0;

void init_msgs(void) {
  if (read_buff != NULL) {
    error("Messages sub-system already initialized.");
  }
  read_buff_size = INIT_BUFF_SIZE;
  read_buff = malloc(read_buff_size * sizeof(char));

  ping = make_msg("ping", NULL);
  ping_str_len = strlen(ping_str);

  throttle = make_msg("throttle", NULL);

  use_turbo = make_msg("turbo", NULL);
  use_turbo_str_len = strlen(use_turbo_str);
}

void close_msgs(void) {
  if (read_buff == NULL) {
    error("Messages sub-system has not been initialized.");
  }
  read_buff_size = 0;
  free(read_buff);
  read_buff = NULL;

  cJSON_Delete(ping);
  cJSON_Delete(throttle);
  cJSON_Delete(use_turbo);
}

cJSON *get_field(cJSON *obj, char *field, char *err_msg) {
  cJSON *the_field = cJSON_GetObjectItem(obj, field);
  if (the_field == NULL) {
    error(err_msg);
  }
  return the_field;
}

cJSON *get_item_at(cJSON *array, int index, char *err_msg) {
  cJSON *the_item = cJSON_GetArrayItem(array, index);
  if (the_item == NULL) {
    error(err_msg);
  }
  return the_item;
}

cJSON *ping_msg(void) {
  return ping;
}

cJSON *join_msg(char *bot_name, char *bot_key) {
  cJSON *data = cJSON_CreateObject();
  char *track_name = getenv("HWO_TRACK");
  if (track_name != NULL) {
    cJSON *bot_id = cJSON_CreateObject();
    cJSON_AddStringToObject(bot_id, "name", bot_name);
    cJSON_AddStringToObject(bot_id, "key", bot_key);
    cJSON_AddItemToObject(data, "botId", bot_id);

    cJSON_AddStringToObject(data, "trackName", track_name);
    cJSON_AddNumberToObject(data, "carCount", 1);

    return make_msg("joinRace", data);
  } else {
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);
    return make_msg("join", data);
  }
}

cJSON *throttle_msg(double value, int at_tick) {
  throttle_val = value;
  throttle_tick = at_tick;

  return throttle;
}

cJSON *turbo_msg(void) {
  return use_turbo;
}

cJSON *make_msg(char *type, cJSON *data) {
  cJSON *json = cJSON_CreateObject();
  cJSON_AddStringToObject(json, "msgType", type);
  if (data != NULL) {
    cJSON_AddItemToObject(json, "data", data);
  }
  return json;
}

cJSON *read_msg(int fd) {
  int readsz;
  char *readp;
  cJSON *json = NULL;

  if (read_buff == NULL) {
    error("Messages sub-system has not been initialized.");
  }

  readsz = 0;
  readp = read_buff;
  while (read(fd, readp, 1) > 0) {
    if (*readp == '\n') {
      break;
    }
    readp++;
    if (++readsz == read_buff_size) {
      printf("WARNING: Message-buffer insufficient - expanding.\n");
      read_buff = realloc(read_buff, read_buff_size + INIT_BUFF_SIZE);
      read_buff_size += INIT_BUFF_SIZE;
      readp = read_buff + readsz;
    }
  }

  if (readsz > 0) {
    *readp = '\0';
    json = cJSON_Parse(read_buff);
    if (json == NULL) {
      error("Malformed JSON (%s): %s", cJSON_GetErrorPtr(), read_buff);
    }
  }

  return json;
}

void write_msg(int fd, cJSON *msg) {
  if (msg == throttle) {
    snprintf(throttle_str, INIT_BUFF_SIZE,
        "{\"msgType\":\"throttle\",\"data\":%f,\"gameTick\":%d}\n",
        throttle_val, throttle_tick);
    write(fd, throttle_str, strlen(throttle_str));
  } else if (msg == ping) {
    write(fd, ping_str, ping_str_len);
  } else if (msg == use_turbo) {
    write(fd, use_turbo_str, use_turbo_str_len);
  } else {
    char nl = '\n';
    char *msg_str = cJSON_PrintUnformatted(msg);
    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);
    free(msg_str);
  }
}

void purge_sent_msg(cJSON *sent_msg) {
  if (sent_msg != ping && sent_msg != throttle && sent_msg != use_turbo) {
    cJSON_Delete(sent_msg);
  }
}
