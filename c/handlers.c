#include "handlers.h"

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "cbot.h"
#include "msgs.h"
#include "utils.h"

/* In gameInit's data->race->cars and carPositions' data arrays, locate the
 * array-item corresponding to our car, if found, else NULL.
 */
static cJSON *find_my_car(cJSON *array) {
  cJSON *child = array->child;
  while (child != NULL) {
    /* Assume "id" is the first child of this array-item. */
    cJSON *id = child->child;
    /* Children of "id" would be "name" and "color". */
    cJSON *id_child = id->child;
    while (id_child != NULL) {
      /* For now, just match on "color". To quote the spec, "Basically it's
       * the color that matters." */
      if (!strcmp(id_child->string, "color") &&
          !strcmp(id_child->valuestring, my_car.color)) {
        return child;
      }
      id_child = id_child->next;
    }
    child = child->next;
  }
  return NULL;
}

cJSON *handle_join(cJSON *msg) {
  puts("Joined the race.");
  return NULL;
}

cJSON *handle_join_race(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data", "Missing data field in joinRace.");
  cJSON *track_name = get_field(msg_data, "trackName",
      "Missing trackName field in data of joinRace.");
  cJSON *car_count = get_field(msg_data, "carCount",
      "Missing carCount field in data of joinRace.");
  printf("Placed on track \"%s\" having a total of %d car(s).\n",
      track_name->valuestring, car_count->valueint);
  return NULL;
}

cJSON *handle_your_car(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data", "Missing data field in yourCar.");
  cJSON *color = get_field(msg_data, "color",
      "No color information in yourCar message.");
  cJSON *name = cJSON_GetObjectItem(msg_data, "name");

  copy_alloced_str(&my_car.color, color->valuestring);
  if (name != NULL) {
    copy_alloced_str(&my_car.name, name->valuestring);
  }
  printf("My car has name \"%s\" and color \"%s\".\n", my_car.name,
      my_car.color);

  return NULL;
}

static void get_track_info(cJSON *track) {
  cJSON *id = get_field(track, "id", "Missing id in track of gameInit.");
  copy_alloced_str(&the_track.id, id->valuestring);

  cJSON *pieces =
    get_field(track, "pieces", "Missing pieces in gameInit message.");
  the_track.num_pieces = cJSON_GetArraySize(pieces);
  if (the_track.num_pieces > 0) {
    int i;
    the_track.pieces = malloc(the_track.num_pieces * sizeof(piece_t));
    for (i = 0; i < the_track.num_pieces; ++i) {
      cJSON *a_piece = get_item_at(pieces, i,
          "Could not retrieve piece from gameInit message.");
      cJSON *length = cJSON_GetObjectItem(a_piece, "length");
      if (length != NULL) {
        the_track.pieces[i].type = STRAIGHT_PIECE;
        the_track.pieces[i].length = length->valuedouble;
        cJSON *is_switch = cJSON_GetObjectItem(a_piece, "switch");
        if (is_switch != NULL && is_switch->type == cJSON_True) {
          the_track.pieces[i].is_switch = 1;
        } else {
          the_track.pieces[i].is_switch = 0;
        }
        the_track.pieces[i].radius = 0.0;
        the_track.pieces[i].angle = 0.0;
      } else {
        cJSON *radius = get_field(a_piece, "radius",
            "Missing radius in bent piece data in gameInit message.");
        cJSON *angle = get_field(a_piece, "angle",
            "Missing angle in bent piece data in gameInit message.");

        the_track.pieces[i].type = BENT_PIECE;
        cJSON *is_switch = cJSON_GetObjectItem(a_piece, "switch");
        if (is_switch != NULL && is_switch->type == cJSON_True) {
          the_track.pieces[i].is_switch = 1;
        } else {
          the_track.pieces[i].is_switch = 0;
        }
        the_track.pieces[i].radius = radius->valuedouble;
        the_track.pieces[i].angle = angle->valuedouble;
        /* The effective length of this bent piece for the car will depend on
         * the lane it is on - inner lanes are shorter than outer lanes. */
        the_track.pieces[i].length =
          arc_len(radius->valuedouble, angle->valuedouble);
      }
    }

    printf("Pieces in track \"%s\":\n", the_track.id);
    for (i = 0; i < the_track.num_pieces; ++i) {
      printf("  %3d: %s - %.2f %s (%.2f, %+.2f)\n", i,
          (the_track.pieces[i].type == STRAIGHT_PIECE) ? "STRT" : "BENT",
          the_track.pieces[i].length,
          (the_track.pieces[i].is_switch == 1) ? "X" : "||",
          the_track.pieces[i].radius, the_track.pieces[i].angle);
    }
  } else {
    snprintf(output_buf, OUTPUT_BUF_SIZE,
        "Invalid number of pieces (%d) in gameInit message.",
        the_track.num_pieces);
    error(output_buf);
  }

  cJSON *lanes =
    get_field(track, "lanes", "Missing lanes in gameInit message.");
  the_track.num_lanes = cJSON_GetArraySize(lanes);
  if (the_track.num_lanes > 0) {
    int i;
    the_track.lane_dists = malloc(the_track.num_lanes * sizeof(double));
    for (i = 0; i < the_track.num_lanes; ++i) {
      cJSON *a_lane = get_item_at(lanes, i,
          "Could not retrieve lane from gameInit message.");
      cJSON *distance_from_center = get_field(a_lane, "distanceFromCenter",
          "Missing distanceFromCenter in lane.");
      cJSON *index = get_field(a_lane, "index", "Missing index in lane.");
      if (index->valueint >= 0 && index->valueint < the_track.num_lanes) {
        the_track.lane_dists[index->valueint] =
          distance_from_center->valuedouble;
      } else {
        snprintf(output_buf, OUTPUT_BUF_SIZE,
            "Invalid lane-index %d in lane information.", index->valueint);
        error(output_buf);
      }
    }

    printf("Lanes in track \"%s\":\n", the_track.id);
    for (i = 0; i < the_track.num_lanes; ++i) {
      printf("  %2d: %.2f\n", i, the_track.lane_dists[i]);
    }
  } else {
    snprintf(output_buf, OUTPUT_BUF_SIZE,
        "Invalid number of lanes %d in gameInit message.", the_track.num_lanes);
    error(output_buf);
  }

  printf("Track \"%s\" has %d pieces with %d lanes each.\n",
      the_track.id, the_track.num_pieces, the_track.num_lanes);

  analyze_track();
}

static void get_cars_info(cJSON *cars) {
  cJSON *the_car = find_my_car(cars);
  if (the_car != NULL) {
    cJSON *dimensions = get_field(the_car, "dimensions",
        "Could not find the dimensions in gameInit message.");
    cJSON *length = get_field(dimensions, "length",
        "Missing length in dimensions in gameInit message.");
    cJSON *width = get_field(dimensions, "width",
        "Missing width in dimensions in gameInit message.");
    cJSON *guide_flag_pos = get_field(dimensions, "guideFlagPosition",
        "Missing guideFlagPosition in dimensions in gameInit message.");
    my_car.length = length->valuedouble;
    my_car.width = width->valuedouble;
    my_car.guide_flag_pos = guide_flag_pos->valuedouble;
    printf("Dimensions of my car: %.2f x %.2f (with guide-flag at %.2f).\n",
        my_car.length, my_car.width, my_car.guide_flag_pos);
  } else {
    error("Could not locate my car in gameInit message.");
  }
}

cJSON *handle_game_init(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data", "Missing data field in gameInit.");
  cJSON *race = get_field(msg_data, "race", "Empty race in gameInit message.");

  /* Assume the track doesn't change between the qualifying round and the
   * race. */
  if (the_track.id == NULL) {
    cJSON *track = get_field(race, "track", "Empty track in gameInit message.");
    get_track_info(track);
  }

  cJSON *cars = get_field(race, "cars", "Empty cars in gameInit message.");
  get_cars_info(cars);

  cJSON *race_session = cJSON_GetObjectItem(race, "raceSession");
  if (race_session != NULL) {
    cJSON *duration_ms = cJSON_GetObjectItem(race_session, "durationMs");
    if (duration_ms != NULL) {
      printf("Qualifying round will last for %d ms.\n", duration_ms->valueint);
    }

    cJSON *laps = cJSON_GetObjectItem(race_session, "laps");
    cJSON *max_lap_time_ms = cJSON_GetObjectItem(race_session, "maxLapTimeMs");
    if (laps != NULL && max_lap_time_ms != NULL) {
      printf("Race will have %d laps with %d ms allowed per lap.\n",
          laps->valueint, max_lap_time_ms->valueint);
    }
    cJSON *quick_race = cJSON_GetObjectItem(race_session, "quickRace");
    if (quick_race != NULL) {
      printf("Quick-race: %s\n",
          (quick_race->type == cJSON_True) ? "true" : "false");
    }
  }

  return NULL;
}

cJSON *handle_game_start(cJSON *msg) {
  reset_car_state(RACING_CAR);
  puts("Race started.");
  return throttle_msg(FULL_THROTTLE, 0);
}

cJSON *handle_turbo_available(cJSON *msg) {
  /* TODO: Utilize turbo-boost. */
  cJSON *msg_data = get_field(msg, "data",
      "Missing data field in turboAvailable.");

  cJSON *turbo_duration_ticks = get_field(msg_data, "turboDurationTicks",
      "Missing turboDurationTicks field in the data for turboAvailable.");
  turbo.expiry_tick = curr_state.tick + turbo_duration_ticks->valueint;

  cJSON *turbo_factor = get_field(msg_data, "turboFactor",
      "Missing turboFactor field in the data for turboAvailable.");
  turbo.boost = turbo_factor->valuedouble;

  printf("Turbo-boost of %.2f available at %d ticks for another %d ticks.\n",
      turbo_factor->valuedouble, curr_state.tick,
      turbo_duration_ticks->valueint);
  return NULL;
}

cJSON *handle_turbo_start(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data",
      "Missing data field in turboStart.");
  cJSON *color = get_field(msg_data, "color",
      "Missing color field in data for turboStart.");
  if (strcmp(color->valuestring, my_car.color)) {
    /* Some other car. */
    return NULL;
  }
  cJSON *game_tick = get_field(msg, "gameTick",
      "Missing gameTick field in turboStart.");

  curr_state.using_turbo = 1;
  printf("Started using turbo-boost at %d ticks.\n", game_tick->valueint);
  return NULL;
}

cJSON *handle_turbo_end(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data",
      "Missing data field in turboEnd.");
  cJSON *color = get_field(msg_data, "color",
      "Missing color field in data for turboEnd.");
  if (strcmp(color->valuestring, my_car.color)) {
    /* Some other car. */
    return NULL;
  }
  cJSON *game_tick = get_field(msg, "gameTick",
      "Missing gameTick field in turboEnd.");

  turbo.boost = DEFAULT_TURBO;
  turbo.expiry_tick = INT_MAX;
  curr_state.using_turbo = 0;
  printf("Turbo-boost expired at %d ticks.\n", game_tick->valueint);
  return NULL;
}

cJSON *handle_lap_finished(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data",
      "Missing data field in lapFinished.");
  cJSON *car = get_field(msg_data, "car",
      "Missing car field in data for lapFinished.");
  cJSON *color = get_field(car, "color",
      "Missing color field in car for lapFinished.");
  if (strcmp(color->valuestring, my_car.color)) {
    /* Some other car. */
    return NULL;
  }
  cJSON *lap_time = get_field(msg_data, "lapTime",
      "Missing lapTime in the data of lapFinished message.");
  cJSON *lap = get_field(lap_time, "lap", "Missing lap number in lapTime.");
  cJSON *ticks = get_field(lap_time, "ticks", "Missing lap number in lapTime.");
  cJSON *millis = get_field(lap_time, "millis",
      "Missing lap number in lapTime.");
  printf("Lap %d finished in %d ticks (%d ms).\n", lap->valueint,
      ticks->valueint, millis->valueint);

  cJSON *ranking = cJSON_GetObjectItem(msg_data, "ranking");
  if (ranking != NULL) {
    cJSON *overall = get_field(ranking, "overall",
        "Missing overall rank in ranking from lapFinished message.");
    printf("Currently ranked at: %d\n", overall->valueint);
  }

  lap_done(lap->valueint, ticks->valueint);
  return NULL;
}

cJSON *handle_finish(cJSON *msg) {
  cJSON *game_tick = cJSON_GetObjectItem(msg, "gameTick");
  if (game_tick != NULL) {
    printf("My car crossed the finish-line at %d game-ticks.\n",
        game_tick->valueint);
  }
  return NULL;
}

cJSON *handle_game_end(cJSON *msg) {
  reset_car_state(IDLE_CAR);
  puts("Race ended.");
  return NULL;
}

cJSON *handle_tournament_end(cJSON *msg) {
  puts("Tournament ended.");
  return NULL;
}

cJSON *handle_dnf(cJSON *msg) {
  puts("Did not finish.");
  return NULL;
}

cJSON *handle_crash(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data", "Missing data field in crash.");
  cJSON *car_color = get_field(msg_data, "color",
      "No car-color in crash message.");
  if (!strcmp(car_color->valuestring, my_car.color)) {
    curr_state.status = CRASHED_CAR;
    cJSON *game_tick = get_field(msg, "gameTick",
        "Missing gameTick field in crash.");
    printf("WARNING: My car crashed at %d ticks.\n", game_tick->valueint);
    printf("Last known slip-angle was %+.2f with speed %.2f at %d ticks.\n",
        curr_state.pos.slip_angle, curr_state.speed, curr_state.tick);
    car_crashed();
  } else {
    puts("Some other car crashed.");
  }
  return NULL;
}

cJSON *handle_spawn(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data", "Missing data field in spawn.");
  cJSON *car_color = get_field(msg_data, "color",
      "No car-color in spawn message.");
  if (!strcmp(car_color->valuestring, my_car.color)) {
    curr_state.status = RACING_CAR;
    cJSON *game_tick = get_field(msg, "gameTick",
        "Missing gameTick field in spawn.");
    printf("My car spawned at %d ticks.\n", game_tick->valueint);
  } else {
    puts("Some other car spawned.");
  }
  return NULL;
}

cJSON *handle_error(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data", "Missing data field in error.");
  if (msg_data == NULL) {
    puts("Unknown server-error.");
  } else {
    printf("SERVER_ERROR: %s\n", msg_data->valuestring);
  }
  return NULL;
}

static void copy_car_state(car_state_t *dest, car_state_t *src) {
  memcpy(dest, src, sizeof(car_state_t));
}

cJSON *handle_car_positions(cJSON *msg) {
  cJSON *msg_data = get_field(msg, "data",
      "Missing data field in carPositions.");
  cJSON *the_car_pos = find_my_car(msg_data);
  if (the_car_pos != NULL) {
    copy_car_state(&last_state, &curr_state);

    cJSON *angle =
      get_field(the_car_pos, "angle", "No angle in carPositions for my car.");
    curr_state.pos.slip_angle = angle->valuedouble;
    curr_state.delta_slip_angle =
      (curr_state.pos.slip_angle - last_state.pos.slip_angle);
    curr_state.delta_delta_slip_angle =
      (curr_state.delta_slip_angle - last_state.delta_slip_angle);

    cJSON *piece_pos = get_field(the_car_pos, "piecePosition",
        "Could not get my car's piece-position.");

    cJSON *piece_index = get_field(piece_pos, "pieceIndex",
        "Could not get the piece-index for my car's piece-position.");
    curr_state.pos.piece_index = piece_index->valueint;
    if (curr_state.pos.piece_index < 0 ||
        curr_state.pos.piece_index >= the_track.num_pieces) {
      snprintf(output_buf, OUTPUT_BUF_SIZE,
          "Invalid track-piece-index %d for my car.",
          curr_state.pos.piece_index);
      error(output_buf);
    }

    cJSON *in_piece_dist = get_field(piece_pos, "inPieceDistance",
        "Could not get the in-piece distance for my car's piece-position.");
    curr_state.pos.in_piece_dist = in_piece_dist->valuedouble;

    cJSON *lane = cJSON_GetObjectItem(piece_pos, "lane");
    if (lane != NULL) {
      cJSON *start_lane_index = get_field(lane, "startLaneIndex",
          "Lane without start lane-index for my car's position.");
      curr_state.pos.start_lane = start_lane_index->valueint;

      cJSON *end_lane_index = get_field(lane, "endLaneIndex",
          "Lane without end lane-index for my car's position.");
      curr_state.pos.end_lane = end_lane_index->valueint;
    }

    curr_state.pos.piece_length = get_piece_length(curr_state.pos.piece_index,
        curr_state.pos.start_lane, curr_state.pos.end_lane);

    cJSON *lap = cJSON_GetObjectItem(piece_pos, "lap");
    if (lap != NULL) {
      curr_state.pos.lap = lap->valueint;
    }
  } else {
    error("Could not locate my car in carPositions message.");
  }

  /* Note: The initial carPositions message doesn't have a gameTick. */
  cJSON *game_tick = cJSON_GetObjectItem(msg, "gameTick");
  if (game_tick != NULL) {
    curr_state.tick = game_tick->valueint;

    if (curr_state.tick > last_state.tick) {
      double dist = get_distance(&last_state.pos, &curr_state.pos);
      curr_state.speed = dist / (curr_state.tick - last_state.tick);
      curr_state.delta_speed = (curr_state.speed - last_state.speed);
    }

    if ((curr_state.tick % TICKS_FOR_HEARTBEAT) == 0) {
      int pi = curr_state.pos.piece_index;
      printf("At %d game-ticks: piece=%d_%s, speed=%.2f, slip-angle=%+.2f\n",
          curr_state.tick, pi,
          (the_track.pieces[pi].type == STRAIGHT_PIECE) ? "S" : "B",
          curr_state.speed, curr_state.pos.slip_angle);
    }
  }

  if ((curr_state.status == IDLE_CAR) || (curr_state.status == CRASHED_CAR)) {
    return ping_msg();
  } else {
    return get_race_response();
  }
}

cJSON *handle_unknown(cJSON *msg) {
  char *msg_str = cJSON_PrintUnformatted(msg);
  printf("WARNING: Unknown message - %s\n", msg_str);
  free(msg_str);
  return ping_msg();
}
