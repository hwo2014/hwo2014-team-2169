#ifndef _CBOT_H_INCLUDED_
#define _CBOT_H_INCLUDED_

#include <stdlib.h>

#include "cJSON.h"

/* Test-servers seem very slow; print out a heartbeat message every N ticks to
 * reassure us that the race is in progress. On the final races, there will be
 * 60 ticks per second. */
#define TICKS_FOR_HEARTBEAT 120

#define DEFAULT_TURBO 1.0

#define NO_THROTTLE 0.0
#define FULL_THROTTLE 1.0

#define next_piece_index(x) (((x) + 1) % the_track.num_pieces)

typedef enum {
  STRAIGHT_PIECE,
  BENT_PIECE,
} piece_type_t;

typedef struct {
  piece_type_t type;
  double length;
  int is_switch;
  double radius;
  double angle;
} piece_t;

typedef struct {
  char *id;
  int num_pieces;
  piece_t *pieces;
  int num_lanes;
  double *lane_dists;
} track_t;

typedef struct {
  char *name;
  char *color;
  double length;
  double width;
  double guide_flag_pos;
} car_t;

typedef enum {
  IDLE_CAR,
  RACING_CAR,
  CRASHED_CAR,
} car_status_t;

typedef struct {
  double slip_angle;
  int piece_index;
  double in_piece_dist;
  double piece_length;
  int start_lane;
  int end_lane;
  int lap;
} car_pos_t;

typedef struct {
  car_status_t status;
  int tick;
  car_pos_t pos;
  double speed;
  double delta_speed;
  double delta_slip_angle;
  double delta_delta_slip_angle;
  int using_turbo;
} car_state_t;

typedef struct {
  double boost;
  int expiry_tick;
} turbo_t;

extern track_t the_track;
extern car_t my_car;
extern turbo_t turbo;

extern car_state_t last_state;
extern car_state_t curr_state;

extern void reset_car_state(car_status_t status);
extern void analyze_track(void);
extern cJSON *get_race_response(void);
extern void lap_done(int lap_num, int in_ticks);
extern void car_crashed(void);

#endif /* _CBOT_H_INCLUDED_ */
