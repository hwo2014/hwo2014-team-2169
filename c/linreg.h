#ifndef _LINREG_H_INCLUDED_
#define _LINREG_H_INCLUDED_

/* A naive learner that uses linear-regression with gradient-descent to
 * approximate the physics of a slot-car interacting with track-pieces.
 *
 * Features selected are based on guesses like drag being proportional to
 * the square or the cube of speed, rolling-resistance being proportional to
 * the speed, centripetal force being proportional to the square of speed by
 * the radius of the lane, etc. */

#define MAX_TRAINING_SAMPLES 128

typedef enum {
  INVALID,
  READY_TO_TRAIN,
  READY_TO_PREDICT,
} model_state_t;

typedef enum {
  /* Dummy feature for theta_0 - must always be used and must be 1.0. */
  UNIT_FEAT = 0,
  SPEED_FEAT,
  SPEED_SQUARED_FEAT,
  DELTA_SPEED_FEAT,
  THROTTLE_FEAT,
  SLIP_ANGLE_FEAT,
  DELTA_SLIP_ANGLE_FEAT,
  DELTA_DELTA_SLIP_ANGLE_FEAT,
  BEND_ANGLE_FEAT,
  INVERSE_RADIUS_FEAT,
  SPEED_SQUARED_BY_INVERSE_RADIUS_FEAT,
  /* Not a feature; just a convenient marker. */
  MAX_FEATURES,
} feature_index_t;

typedef struct {
  double x[MAX_FEATURES];
} features_t;

/* Used for normalization of features to make them lie between 0.0 and 1.0:
 *   x = (x - min) / range
 * so that gradient-descent can converge in a reasonable time.
 */
typedef struct {
  double minimum;
  double range;
} min_and_range_t;

typedef struct {
  min_and_range_t minima_and_ranges[MAX_FEATURES];
} feature_scale_t;

typedef enum {
  OBS_SPEED = 0,
  OBS_SLIP_ANGLE,
  /* Not an observation; just a convenient marker. */
  MAX_OBS,
} obs_index_t;

typedef struct {
  features_t features;
  double obs[MAX_OBS];
} sample_t;

typedef struct {
  sample_t samples[MAX_TRAINING_SAMPLES];
  int num_samples;
} sample_data_t;

typedef struct {
  double t[MAX_FEATURES];
} thetas_t;

typedef struct {
  /* Not owned. */
  char *name;
  model_state_t state;

  int num_used_features;
  feature_scale_t scale;
  thetas_t speed_thetas;
  thetas_t slip_angle_thetas;
} model_t;

/* Initialize the given model to use the given sub-set of features. */
extern void init_model(model_t *model, char *model_name, int num_features);

/* Clean up the model. */
extern void clean_model(model_t *model);

/* Train the given model using the given training-data. */
extern void train_model(model_t *model, sample_data_t *samples);

/* Predict the speed and slip-angle using the given model and features. */
extern void predict(model_t *model, features_t *features, double *speed,
    double *slip_angle);

#endif /* _LINREG_H_INCLUDED_ */
