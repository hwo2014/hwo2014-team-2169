#include "linreg.h"

#include <float.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"

#define EPSILON 0.0001
#define INIT_THETA 1.0

#define MAX_GRAD_DESC_ITERS 1024

/* Set aside this fraction of the training-data from the end for
 * cross-validation. Ideally we should also have a separate test data-set. */
#define CV_DATA_PERCENT 0.15

#define copy_thetas(d,s) memcpy(d, s, sizeof(thetas_t))

/* Uncomment the following to get verbose messages useful for debugging. */
/* #define VERBOSE_LINREG 1 */

typedef struct {
  /* Not owned. */
  char *objective;

  sample_t *samples;
  obs_index_t obs_index;
  int begin_index;
  int num_items;

  thetas_t *thetas;
  double alpha;
  double lambda;
} training_data_t;

void init_model(model_t *model, char *model_name, int num_features) {
  if (num_features <= 0 || num_features > MAX_FEATURES) {
    snprintf(output_buf, OUTPUT_BUF_SIZE, "Invalid number of features %d.",
        num_features);
    error(output_buf);
  }

  model->name = model_name;
  model->num_used_features = num_features;
  model->state = READY_TO_TRAIN;
}

void clean_model(model_t *model) {
  model->name = NULL;
  model->num_used_features = 0;
  model->state = INVALID;
}

/* Scale the features so that they are roughly between 0 and 1.0. */
static void scale_features(model_t *model, features_t *features) {
  feature_scale_t *scale = &(model->scale);
  int i;
  for (i = 0; i < model->num_used_features; ++i) {
    if (scale->minima_and_ranges[i].range > EPSILON) {
      features->x[i] = (features->x[i] - scale->minima_and_ranges[i].minimum) /
        scale->minima_and_ranges[i].range;
    } else {
      /* All values are essentially the same (e.g. 1.0 for the dummy unit
       * feature). Use the only sensible non-zero scaled value. */
      features->x[i] = 1.0;
    }
  }
}

static void scale_sample_data(model_t *model, training_data_t *training_data,
    training_data_t *cv_data) {
  feature_scale_t *scale = &(model->scale);
  double maxima[MAX_FEATURES];
  int i;

  /* Find the minima and the maxima in the training data. */
  for (i = 0; i < model->num_used_features; ++i) {
    scale->minima_and_ranges[i].minimum = DBL_MAX;
    maxima[i] = DBL_MIN;
  }
  for (i = 0; i < training_data->num_items; ++i) {
    int j = training_data->begin_index + i;
    int k;
    /* Sanity check the unit-feature, since we depend on this. */
    if (training_data->samples[j].features.x[UNIT_FEAT] != 1.0) {
      snprintf(output_buf, OUTPUT_BUF_SIZE,
          "Unit feature not 1.0 in sample-data %d for model %s.\n", j,
          model->name);
      error(output_buf);
    }
    for (k = 0; k < model->num_used_features; ++k) {
      scale->minima_and_ranges[k].minimum =
        min_of(scale->minima_and_ranges[k].minimum,
            training_data->samples[j].features.x[k]);
      maxima[k] = max_of(maxima[k], training_data->samples[j].features.x[k]);
    }
  }

  /* Use the minima and the maxima to find the ranges in the training data. */
  for (i = 0; i < model->num_used_features; ++i) {
    scale->minima_and_ranges[i].range =
      maxima[i] - scale->minima_and_ranges[i].minimum;
  }

  /* Scale the features of the training data based on the minima and ranges. */
  for (i = 0; i < training_data->num_items; ++i) {
    int j = training_data->begin_index + i;
    scale_features(model, &(training_data->samples[j].features));
  }

  /* Scale the features of the cross-validation data based on the training
   * data. */
  for (i = 0; i < cv_data->num_items; ++i) {
    int j = cv_data->begin_index + i;
    /* Sanity check the unit-feature, since we depend on this. */
    if (cv_data->samples[j].features.x[UNIT_FEAT] != 1.0) {
      snprintf(output_buf, OUTPUT_BUF_SIZE,
          "Unit feature not 1.0 in sample-data %d for model %s.\n", j,
          model->name);
      error(output_buf);
    }
    scale_features(model, &(cv_data->samples[j].features));
  }
}

/* h(x) = t0 + t1*x1 + t2*x2 +... */
static double get_hypothesis(model_t *model, features_t *features,
    thetas_t *thetas) {
  double ret_val = 0.0;
  int i;
  for (i = 0; i < model->num_used_features; ++i) {
    ret_val += thetas->t[i] * features->x[i];
  }
  return ret_val;
}

/* J(theta) = (1 / (2 * m)) * (SUM((h(x) - y) ^ 2) + lambda * SUM(theta^2)) */
static double get_cost(model_t *model, training_data_t *data) {
  double sum = 0.0;
  int i;
  for (i = 0; i < data->num_items; ++i) {
    int j = data->begin_index + i;
    double hypothesis =
      get_hypothesis(model, &(data->samples[j].features), data->thetas);
    double error = hypothesis - data->samples[j].obs[data->obs_index];
    sum += error * error;
  }
  if (data->lambda > 0.0) {
    double theta_sq_sum = 0.0;
    /* Do not penalize UNIT_FEAT. */
    for (i = 1; i < model->num_used_features; ++i) {
      theta_sq_sum += data->thetas->t[i] * data->thetas->t[i];
    }
    sum += data->lambda * theta_sq_sum;
  }
  return sum / (2.0 * data->num_items);
}

/* Execute a single step of gradient-descent by simultaneously updating
 * each theta:
 *   t(j) = t(j) * (1 - alpha * lambda / m)
 *     - (alpha / m) * SUM((h(x) - y) * x(j))
 */
static void gradient_descent_step(model_t *model, training_data_t *data) {
  double theta_deltas[MAX_FEATURES];
  double theta_reg_factor = 1.0;
  int i;
  for (i = 0; i < model->num_used_features; ++i) {
    double sum = 0.0;
    int j;
    for (j = 0; j < data->num_items; ++j) {
      int k = data->begin_index + j;
      features_t *a_sample = &(data->samples[k].features);
      double hypothesis = get_hypothesis(model, a_sample, data->thetas);
      sum +=
        (hypothesis - data->samples[k].obs[data->obs_index]) * a_sample->x[i];
    }
    theta_deltas[i] = data->alpha * sum / data->num_items;
  }
  if (data->lambda > 0.0) {
    theta_reg_factor -= data->lambda * data->alpha / data->num_items;
  }
  for (i = 0; i < model->num_used_features; ++i) {
    data->thetas->t[i] *= theta_reg_factor;
    data->thetas->t[i] -= theta_deltas[i];
  }
}

static void gradient_descent(model_t *model, training_data_t *data) {
  double prev_cost;
  prev_cost = get_cost(model, data);

#ifdef VERBOSE_LINREG
  printf("Gradient-descent costs for %d samples with learning-rate %.5f: %.5f",
      data->num_items, data->alpha, prev_cost);
#endif /* VERBOSE_LINREG */

  int i;
  for (i = 0; i < MAX_GRAD_DESC_ITERS; ++i) {
    double curr_cost;
    gradient_descent_step(model, data);
    curr_cost = get_cost(model, data);

#ifdef VERBOSE_LINREG
    printf(" -> %.5f", curr_cost);
#endif /* VERBOSE_LINREG */

    if (fabs(curr_cost - prev_cost) < EPSILON) {
      break;
    }
    prev_cost = curr_cost;
  }
#ifdef VERBOSE_LINREG
  printf(" (%d iterations).\n", i);

  if (i < MAX_GRAD_DESC_ITERS) {
    printf("Gradient-descent converged in %d iterations.\n", i);
  } else {
    printf("WARNING: Gradient-descent did not converge after %d iterations.\n",
        i);
  }
#endif /* VERBOSE_LINREG */
}

static void run_gradient_descent(model_t *model,
    training_data_t *training_data, training_data_t *cv_data) {
  double alphas[] = {0.30, 0.03, 0.01};
  int num_alphas = sizeof(alphas)/sizeof(alphas[0]);
  double lambdas[] = {0.0, 3.0, 30.0};
  int num_lambdas = sizeof(lambdas)/sizeof(lambdas[0]);
  double min_cost = DBL_MAX, min_alpha = 0.0, min_lambda = 0.0;
  thetas_t min_thetas;
  struct timeval start_time, end_time;
  int i, j;

  for (i = 0; i < model->num_used_features; ++i) {
    training_data->thetas->t[i] = INIT_THETA;
  }
  cv_data->alpha = 0.0;
  cv_data->lambda = 0.0;
  gettimeofday(&start_time, NULL);
  for (i = 0; i < num_lambdas; ++i) {
    training_data->lambda = lambdas[i];
    for (j = 0; j < num_alphas; ++j) {
      double cv_cost;
      training_data->alpha = alphas[j];
      gradient_descent(model, training_data);
      cv_cost = get_cost(model, cv_data);
      if (cv_cost < min_cost) {
        min_cost = cv_cost;
        min_alpha = training_data->alpha;
        min_lambda = training_data->lambda;
        copy_thetas(&min_thetas, training_data->thetas);
      }
    }
  }
  copy_thetas(training_data->thetas, &min_thetas);
  gettimeofday(&end_time, NULL);
  printf("Gradient-descent for %s, %s:\n", model->name,
      training_data->objective);
  printf("  alpha=%.2f, lambda=%.2f, CV-cost=%.5f in %ld us.\n",
      min_alpha, min_lambda, min_cost, time_diff_us(&end_time, &start_time));
}

/* NOTE: Assumes features are already scaled. */
static void predict_internal(model_t *model, features_t *features,
    double *speed, double *slip_angle) {
  *speed = get_hypothesis(model, features, &model->speed_thetas);
  *slip_angle = get_hypothesis(model, features, &model->slip_angle_thetas);
}

#ifdef VERBOSE_LINREG
static void print_model_and_samples(model_t *model,
    sample_data_t *sample_data) {
  int i;
  printf("Scaled sample data:\n");
  for (i = 0; i < sample_data->num_samples; ++i) {
    features_t *a_sample = &(sample_data->samples[i].features);
    int j;
    printf("  %3d: ", i);
    for (j = 0; j < model->num_used_features; ++j) {
      printf("%.2f ", a_sample->x[j]);
    }
    printf("(sp=%.2f, sa=%.2f)\n", sample_data->samples[i].obs[OBS_SPEED],
        sample_data->samples[i].obs[OBS_SLIP_ANGLE]);
  }
  printf("Speed thetas:\n");
  for (i = 0; i < model->num_used_features; ++i) {
    printf("  %.2f", model->speed_thetas.t[i]);
  }
  printf("\nSlip-angle thetas:\n");
  for (i = 0; i < model->num_used_features; ++i) {
    printf("  %.2f", model->slip_angle_thetas.t[i]);
  }
  printf("\n");
}
#endif /* VERBOSE_LINREG */

static void test_predictions(model_t *model, sample_data_t *sample_data,
    int num_training_samples) {
  int i;
  printf("Predictions of model \"%s\":\n", model->name);
  for (i = num_training_samples; i < sample_data->num_samples; ++i) {
    features_t *features = &(sample_data->samples[i].features);
    double speed, slip_angle;
    predict_internal(model, features, &speed, &slip_angle);
    printf("  speed=%.2f (vs %.2f), slip-angle=%+.2f (vs %+.2f)\n",
        speed, sample_data->samples[i].obs[OBS_SPEED], slip_angle,
        sample_data->samples[i].obs[OBS_SLIP_ANGLE]);
  }
}

void train_model(model_t *model, sample_data_t *sample_data) {
  int num_training_samples;

  if (model->state != READY_TO_TRAIN) {
    error("Model has already been trained or is invalid.");
  }

  num_training_samples =
    floor((1.0 - CV_DATA_PERCENT) * (double)sample_data->num_samples);
  if (num_training_samples < model->num_used_features) {
    int min_needed =
      ceil((double)model->num_used_features / (1.0 - CV_DATA_PERCENT));
    snprintf(output_buf, OUTPUT_BUF_SIZE,
        "Need at least %d samples to train the model.", min_needed);
    error(output_buf);
  }

  {
    training_data_t training_data;
    training_data_t cv_data;

    training_data.samples = sample_data->samples;
    training_data.begin_index = 0;
    training_data.num_items = num_training_samples;

    cv_data.samples = sample_data->samples;
    cv_data.begin_index = num_training_samples;
    cv_data.num_items = sample_data->num_samples - num_training_samples;

    training_data.objective = "speed";
    training_data.thetas = &(model->speed_thetas);
    training_data.obs_index = OBS_SPEED;
    cv_data.objective = "speed";
    cv_data.thetas = &(model->speed_thetas);
    cv_data.obs_index = OBS_SPEED;

    scale_sample_data(model, &training_data, &cv_data);
    run_gradient_descent(model, &training_data, &cv_data);

    training_data.objective = "slip_angle";
    training_data.thetas = &(model->slip_angle_thetas);
    training_data.obs_index = OBS_SLIP_ANGLE;
    cv_data.objective = "slip_angle";
    cv_data.thetas = &(model->slip_angle_thetas);
    cv_data.obs_index = OBS_SLIP_ANGLE;
    run_gradient_descent(model, &training_data, &cv_data);
  }

#ifdef VERBOSE_LINREG
  print_model_and_samples(model, sample_data);
#endif /* VERBOSE_LINREG */
  test_predictions(model, sample_data, num_training_samples);

  model->state = READY_TO_PREDICT;
}

void predict(model_t *model, features_t *features, double *speed,
    double *slip_angle) {
  if (model->state != READY_TO_PREDICT) {
    error("Model has not yet been trained or is invalid.");
  }
  scale_features(model, features);
  predict_internal(model, features, speed, slip_angle);
}
