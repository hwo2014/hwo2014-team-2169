#ifndef _HANDLERS_H_INCLUDED_
#define _HANDLERS_H_INCLUDED_

#include <stdlib.h>

#include "cJSON.h"

extern cJSON *handle_car_positions(cJSON *msg);
extern cJSON *handle_join(cJSON *msg);
extern cJSON *handle_join_race(cJSON *msg);
extern cJSON *handle_your_car(cJSON *msg);
extern cJSON *handle_game_init(cJSON *msg);
extern cJSON *handle_game_start(cJSON *msg);
extern cJSON *handle_turbo_available(cJSON *msg);
extern cJSON *handle_turbo_start(cJSON *msg);
extern cJSON *handle_turbo_end(cJSON *msg);
extern cJSON *handle_lap_finished(cJSON *msg);
extern cJSON *handle_finish(cJSON *msg);
extern cJSON *handle_game_end(cJSON *msg);
extern cJSON *handle_tournament_end(cJSON *msg);
extern cJSON *handle_dnf(cJSON *msg);
extern cJSON *handle_crash(cJSON *msg);
extern cJSON *handle_spawn(cJSON *msg);
extern cJSON *handle_error(cJSON *msg);
extern cJSON *handle_unknown(cJSON *msg);

#endif /* _HANDLERS_H_INCLUDED_ */
